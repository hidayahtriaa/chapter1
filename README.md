# Chapter1 & Chapter3 (Binar Car Rental - Responsive Design)

CONTENTS OF THIS FILE
---------------------

 * Description
 * Requirements
 * Installation Guide
 * FAQ


## Description

This project is created for Binar Academy's challenge. The design of the web is build based on [this prototype design](https://www.figma.com/file/QiNXZPX7OwUeFzqSPuiQBE/BCR---Binar-Car-Rental). The aim of this project to make a responsive website that we could access with using mobile phone, tablet, and desktop. To build this website, we combine HTML, CSS, and Javascript (Bootstrap). Based on the prototype design, the web contains some sections, such as Navigation Bar, Hero Section, Our Services, Why Us, Testimonial, Banner, Frequently Asked Questions, and Footer.

In this website, we used HTML code to structure the web page and its content. Then, to describing the presentation of web pages, including colors, layouts, and fonts, we used CSS, also we used Bootstrap 5 as CSS Framework to add more class or elements where the cdn accessed online. 

## Requirements
- Code Editor (VSCode, Sublime Text, etc.)
- [Bootstrap V-5](https://getbootstrap.com/)

## Installation Guide
 1. Download repository files or clone from [this repository](https://gitlab.com/hidayahtriaa/chapter1)
 2. Open the files in Code Editor
 3. Run the index.html file


## FAQ
For any questions and further informations, you can contact me through email.